
const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0);
            const greenHex = converter.rgbToHex(0,255,0);
            const blueHex = converter.rgbToHex(0,0,255);
            
            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            var r = converter.hexToRgb("ff0000");
            var g = converter.hexToRgb("00ff00");
            var b = converter.hexToRgb("0000ff")
            
            expect(r).to.equal("255,0,0")
            expect(g).to.equal("0,255,0")
            expect(b).to.equal("0,0,255")
        })
    })
    
})